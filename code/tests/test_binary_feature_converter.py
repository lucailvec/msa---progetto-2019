import unittest
import pandas as pd

from src.msa.binary_feature_converter import Binary_feature_converter

df_a = pd.DataFrame({
    'A': [1, 2, 3, 4, 5],
    'y_wrong_1': [0, 1, 0, 1, 2],
    'y_wrong_2': [0, 0, -1, 0, 0],
    'y_wrong_1hot_a': [1, 0, 1, 0, 1],
    'y_wrong_1hot_b': [0, 1, 0, 1, 1],
    'y_0': [0, 1, 1, 1, 0],
    'y_1': [1, 0, 0, 1, 0]}
)

df_b = pd.DataFrame({
    'A': [0, 2, 3, 4, 1],
    'y_0': [0, 1, 1, 1, 0],
    'y_1': [1, 0, 0, 0, 0]}
)
class Test_binary_feature_converter(unittest.TestCase):


    #test correct init
    def test_pos_class(self):
        with self.assertRaises(AssertionError):
            Binary_feature_converter(pos_class=[12,3,4,5])
    def test_class_values(self):
        with self.assertRaises(AssertionError):
            Binary_feature_converter(class_values=[1,2,3,4])
    def test_class_values_2(self):
        with self.assertRaises(TypeError):
            Binary_feature_converter(class_values=1)
    def test_y_name(self):
        with self.assertRaises(AssertionError):
            Binary_feature_converter(y_new_name=['ciao', 'mondo'])
    def test_features_collapse(self):
        with self.assertRaises(AssertionError):
            Binary_feature_converter(y_names=[])
    def test_features_collapse_2(self):
        with self.assertRaises(AssertionError):
            Binary_feature_converter(y_names="")

    #test convert
    def test_missing_target_column(self):
        bfc = Binary_feature_converter(class_values=[1, -1], y_new_name='new_ouput',y_names=['y_wrong_1hot_a','y_wrong_1hot_b'])
        with self.assertRaises(AssertionError):
            bfc.convert(df_a,target_variable='B')
    def test_wrong_one_hot(self):
        bfc = Binary_feature_converter(class_values=[1, -1], y_new_name='new_ouput',y_names=['y_wrong_1hot_a','y_wrong_1hot_b'])
        with self.assertRaises(AssertionError):
            bfc.convert(df_a,target_variable='A')

    def test_positive_class(self):
        new_name = 'aodnao'
        bfc = Binary_feature_converter(class_values=[1, -1], y_new_name=new_name,y_names=['A','y_0','y_1'])
        num_cols = len(df_b.columns)
        new_df = bfc.convert(df_b,target_variable='A',replace=False)

        new_columns_name = new_df.columns
        self.assertTrue( len(new_columns_name) == num_cols + 1)
        self.assertTrue(new_name in new_columns_name)

    # todo manca da testare correttezza dell'output
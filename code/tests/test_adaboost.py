import unittest

from sklearn.model_selection import StratifiedKFold

from adaboost import Adaboost
from binary_feature_converter import Binary_feature_converter
from stump import *

from import_data import get_data_frame

num_element = 12
y = np.array(np.random.randint(2, size=num_element))
w_1 = 1 / num_element + np.zeros(num_element)
w_2 = np.random.chisquare(2, size=num_element)
w_2 = w_2 / np.sum(w_2)

flip_1 = np.random.choice(num_element)
if flip_1 + 1 < num_element:
    other_flip = flip_1 + 1
else:
    other_flip = flip_1 - 1

y_1 = y.copy()
y_1[flip_1] = 1 - y_1[flip_1]

y_2 = y_1.copy()
y_2[other_flip] = 1 - y_2[other_flip]

y_1_b = y.copy()
y_1_b[other_flip] = 1 - y_1_b[other_flip]

y_perturb = np.random.chisquare(2, size=num_element)


class Test_adaboost(unittest.TestCase):
    def test_base_after_one_round(self):
        df = pd.DataFrame({'y': y, 'y_2': y_2})
        boost = Adaboost(max_round=1, learning_algo=Stump)
        bfc = Binary_feature_converter(pos_class=1, class_values=[+1, -1], y_new_name='y_parsed', y_names=['y'])

        df = bfc.convert(df=df, target_variable='y', replace=True)
        boost.train(X=df.drop(['y_parsed'], axis=1).to_numpy(), y=df['y_parsed'].to_numpy())
        w_1 = boost.weight[flip_1]
        w_2 = boost.weight[other_flip]
        self.assertEqual(w_1, w_2)
        self.assertTrue(np.all(w_1 >= boost.weight) or np.all(w_2 >= boost.weight),
                        f'w_1: {w_1}, w_2:{w_2}\nAll weight: {boost.weight}')
        self.assertTrue(np.isclose(boost.weight.sum(), 1.))
        preds = np.array([boost.predict(rows.to_numpy()) for i, rows in df.iterrows()])
        error = np.sum(preds != df['y_parsed'])
        self.assertEqual(2, error, f'y_2  {list(df["y_2"])}\npreds {preds}\ny_par {list(df["y_parsed"])}')

    def test_with_a_less_pertubated_variable(self):
        df = pd.DataFrame({'y': y, 'y_1': y_1, 'y_2': y_2})
        boost = Adaboost(max_round=5, learning_algo=Stump)
        bfc = Binary_feature_converter(pos_class=1, class_values=[+1, -1], y_new_name='y_parsed', y_names=['y'])

        df = bfc.convert(df=df, target_variable='y', replace=True)
        _X = df.drop(['y_parsed'], axis=1).to_numpy()
        _y = df['y_parsed'].to_numpy()

        boost.train(X=_X, y=_y)
        preds = boost.predict(_X)

        error = np.sum(preds != _y)
        self.assertEqual(error, 1)

    def test_with_a_less_pertubated_variable(self):
        df = pd.DataFrame({'y': y,
                           'y_1': y_1,
                           'y_2': y_2,
                           'y_1_b': y_1_b})

        boost = Adaboost(max_round=5, learning_algo=Stump)
        bfc = Binary_feature_converter(pos_class=1, class_values=[+1, -1], y_new_name='y_parsed', y_names=['y'])

        df = bfc.convert(df=df, target_variable='y', replace=True)
        _X = df.drop(['y_parsed','y_1_b'], axis=1).to_numpy()
        _y = df['y_parsed'].to_numpy()

        boost.train(X=_X, y=_y)
        preds = np.array(_X)

        error = np.sum(preds != y)
        self.assertEqual(0, 0)

    def test_iterator_on_train_methods(self):
        df = pd.DataFrame({'y': y, 'y_1': y_1, 'y_2': y_2, 'y_1_b': y_1_b})
        max_round = 25
        boost = Adaboost(max_round=max_round, learning_algo=Stump)
        bfc = Binary_feature_converter(pos_class=1, class_values=[+1, -1], y_new_name='y_parsed', y_names=['y'])

        df = bfc.convert(df=df, target_variable='y', replace=True)
        for nth_round, sub_ada in enumerate(
                boost.train(X=df.drop(['y_parsed'], axis=1).to_numpy(), y=df['y_parsed'].to_numpy(), stops_at_round=True,
                            save_statistics=True)):
            length = nth_round + 1

        self.assertEqual(max_round, len(sub_ada.weight_distribution))
        self.assertEqual(max_round, len(sub_ada.margin_distribution))
        self.assertEqual(max_round, len(sub_ada.hypotesis))

    def test_dataSet(self):

        round_max = 5
        random_state = 42
        i_n_fold = 3
        path = '../data/'
        file_name = 'forest-cover-type.csv'

        df = get_data_frame(path + file_name)
        cols_label = [col_name for col_name in df.columns if 'Soil_Type' in col_name]
        bfc = Binary_feature_converter(class_values=[1, -1], y_new_name='y', y_names=cols_label)
        dfs = [bfc.convert(df=df, target_variable=col_name, replace=True) for col_name in cols_label]

        i_skf = StratifiedKFold(n_splits=i_n_fold, random_state=random_state, shuffle=True)

        valid_error = []
        ada = Adaboost(max_round=round_max, learning_algo=Stump)

        X = dfs[0].drop(['y'], axis=1).to_numpy()
        y = dfs[0]['y'].to_numpy()
        for i_nth, i_other in enumerate(i_skf.split(X, y)):
            i_train_index, validation_index = i_other
            X_i_train, X_val = X[i_train_index], X[validation_index]
            y_i_train, y_val = y[i_train_index], y[validation_index]

            for t in ada.train(X=X_i_train, y=y_i_train, save_statistics=False, stops_at_round=True):
                valid_error.append((t, (t.predict(X_val) != y_val).sum() / len(y_val)))

            break

        print("!")

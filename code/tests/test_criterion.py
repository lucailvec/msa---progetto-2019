import unittest
import pandas as pd
from src.msa.criterion import *

margin = 0.01
num_element = 10000

class test_criterion(unittest.TestCase):
    def test_base(self):
        y = np.array(np.random.randint(2, size=num_element))
        w_1 = 1 / num_element + np.zeros(num_element)
        w_2 = np.random.chisquare(2, size=num_element)
        w_2 = w_2 / np.sum(w_2)
        x = np.random.chisquare(2, size=num_element)

        df = pd.DataFrame({'y': y, 'x': x, 'w_1': w_1, 'w_2': w_2})

        # partiziono in base a criterio gli elementi:

        crit = lambda x: x > 3
        df['partition'] = df['x'].apply(crit)
        df_A = df[df['partition'] == True]
        df_B = df[df['partition'] == False]

        # nel caso di equiprobabilità
        classica = gini(np.array([len(df_A) / len(df), len(df_B) / len(df)]))
        pesata = caos_index(weight=w_1, label=df['partition'], criterion='gini')
        print(classica, pesata)
        self.assertTrue(abs(classica - pesata) < margin)

    def test_resampling(self):
        y = np.array(np.random.randint(2, size=num_element))
        w_1 = 1 / num_element + np.zeros(num_element)
        w_2 = np.random.chisquare(2, size=num_element)
        w_2 = w_2 / np.sum(w_2)
        x = np.random.chisquare(2, size=num_element)

        df = pd.DataFrame({'y': y, 'x': x, 'w_1': w_1, 'w_2': w_2})

        # partiziono in base a criterio gli elementi:

        crit = lambda x: x > 3
        df['partition'] = df['x'].apply(crit)

        new_df = np.random.choice(num_element, size=num_element, replace=True, p=df['w_2'])
        resampled_df = df.loc[new_df]
        p_classica_resampled = np.array([
            len(resampled_df[resampled_df['partition'] == False]) / len(resampled_df),
            len(resampled_df[resampled_df['partition'] == True]) / len(resampled_df),
        ])
        classica = gini(p_classica_resampled)
        # nel caso di equiprobabilità
        pesata = caos_index(weight=w_2, label=df['partition'], criterion='gini')
        print(classica, pesata)
        self.assertTrue(abs(classica - pesata) < margin)

    def test_caos_half(self):
        y = np.array(np.random.randint(2, size=num_element))

        df = pd.DataFrame({'y': y, 'x': y})


        self.assertTrue(abs(0.5-caos_index(label=df['y'], criterion='gini'))<margin)
    def test_zero(self):
        y = np.zeros(10,dtype=np.int8)
        self.assertEqual(0,caos_index(label=y))

    def test_weighted_base(self):
        y=np.random.randint(2, size=10)
        w=np.zeros(10) + 1/10
        self.assertEqual(caos_index(label=y), caos_index(weight=w,label=y))

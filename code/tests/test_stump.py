import unittest
import numpy as np
import pandas as pd

from stump import Stump

num_element = 10
y = np.array(np.random.randint(2, size=num_element))*2 -1
w_1 = 1 / num_element + np.zeros(num_element)
w_2 = np.random.chisquare(2, size=num_element)
w_2 = w_2 / np.sum(w_2)
x = np.random.chisquare(2, size=num_element)
df = pd.DataFrame({'y': y, 'x': x, 'w_1': w_1, 'w_2': w_2})

first_flip = np.random.choice(num_element)
y_1 = y.copy()
y_1[first_flip] = - y_1[first_flip]
y_2 = y_1.copy()
second_flip = np.random.choice(num_element)
y_2[second_flip] = - y_2[second_flip]


class Test_stump(unittest.TestCase):
    def test_not_explode(self):
        X = df.drop(['y', 'w_1', 'w_2'], axis=1).to_numpy()
        y = df['y'].to_numpy()
        stump = Stump()
        weight = np.ones(len(X))/len(X)
        stump.train(X, y,weight)

    def test_obviously_the_right_one(self):
        """
        L'unica feature perfetta è ovviamente quella della label.
        Tra tutte le colonne viene scelta quella e con una soglia dello 0.5
        """
        X = df
        y = df['y'].to_numpy()
        stump = Stump()
        stump.train(X.to_numpy(), y,weight=np.ones(len(X))/len(X))
        self.assertEqual('y', X.columns[stump.column_name])
        self.assertTrue(-1.0 == stump.threshold or 0.0 == stump.threshold)

    def test_less_obvious_one(self):
        X = pd.DataFrame({ 'y_1': y_1, 'y_2': y_2})
        weight = np.ones(len(X))/len(X)
        stump = Stump()
        stump.train(X.to_numpy(), y ,weight=weight)
        self.assertEqual(X.columns[stump.column_name], 'y_1')

    def test_almost_correct(self):
        X = pd.DataFrame({'y_2':y_2})
        weight = np.ones(len(X)) / len(X)
        stump = Stump()
        stump.train(X.to_numpy(),y, weight)
        self.assertGreater(sum( stump.predict(y_2) == y) , len(y) - sum(stump.predict(y_2) != y) -1 )
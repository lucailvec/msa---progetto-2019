import logging
from typing import Sequence, Optional, List, Tuple

import numpy as np
import pandas as pd
from joblib import Parallel, delayed

eps = 0.0000001    #np.finfo(float).eps #0.001 #per controllare di essere lontano da 0.5

class Model:
    def __init__(self, weighted, randomized):
        pass

    def predict(self, x: Sequence[any]) -> any:
        pass

    def train(self, X: pd.DataFrame, y: Sequence[any], weight: Optional[Sequence[float]]):
        pass

    @staticmethod
    def from_1D_arr_to_thr(a):
        '''
        La funzione prende una variabile (ndarray) e calcola le soglie possibili per la classificazione.
        '''

        a = np.unique(a)
        b = np.empty(len(a) + 1)

        b[0:2] = [a[0], a[-1]]
        b[2:] = (a[1:] + a[0:-1]) / 2
        return b


class Stump(Model):
    """
    Se non passo i pesi durante la fase di init, questo è uno stump che ragiona in modo classico.
    Se invece glieli passo lavora come frequentista e andrà a calcolare l'indici di caos con la distribuzione dei pesi.

    """

    def __init__(self, randomized=True):  # todo trasformare tipo da attributo a getter
        """

        :param randomized:
        :param weight: the weight of
        :param randomized:
        """
        self.column_name = None
        self.threshold = None
        self.left_label = None
        self.right_label = None


    def train(self, X: np.ndarray, y: List[any], weight: List[float], criterion: str = 'gini'):
        unique_values = np.unique(y)
        assert len(unique_values)==2,'y needs to be discrete values, only two class'

        #caos_pre_split = caos_index(event=unique_values, weight=weight, label=y, criterion=criterion)

        # [x] caos prima di fare lo split che lo faccio guardando l'y
        # [x]caos dopo aver fatto lo split:
        # - decido la soglia
        # - separo i due insiemi
        # - calcolo la media pesata del caos dei due insiemi
        # - calcolo l'information gain
        # - salvo un po' di dati

        n_cols = X.shape[1]

        self.left_label,self.right_label = unique_values

        def _inner_funz(col_name):
            column = X[:, col_name]
            threshold = Model.from_1D_arr_to_thr(column)

            # prendo il migliore e salvo i dati su un altra struttura
            gen = [[
                self.__eval_error(unique_values, column, t, weight, y),
                t
            ] for t in threshold]

            _best_ig, _best_t = min(gen, key=lambda x: abs(x[0]))
            return _best_ig, _best_t, col_name

        best_res = Parallel(n_jobs=4, backend='loky')(delayed(_inner_funz)(col_name) for col_name in range(n_cols))

        # best col
        best_res = np.array(sorted(best_res, key= lambda x: abs(x[0])  ))

        #print(best_res)

        index = 0
        while True:
            self.best_ig, self.threshold, self.column_name = best_res[index,:]
            self.column_name = int(self.column_name)

            #print(f'{self}')
            if abs(0.5 - abs(self.best_ig))<eps:

                index+=1
                if index == best_res.shape[0]:
                    raise Exception("No param far almost eps from 0.5")
            else:
                if self.best_ig < 0:
                    #print("Scambio foglie")
                    _temp = self.right_label
                    self.right_label = self.left_label
                    self.left_label = _temp
                    self.best_ig *= -1
                break


    def predict(self, x: Sequence[any],t = None,labels: Tuple[any] = None, col_name : int = None) -> any:
        if isinstance(x, np.ndarray):
            if len(x.shape) == 2:
                f = np.vectorize(lambda _x: self.left_label if _x <= self.threshold else self.right_label)
                return f(x[:, self.column_name])
            else:
                return self.left_label if x[self.column_name] <= self.threshold else self.right_label
        else:
            return self.left_label if x <= self.threshold else self.right_label

    @staticmethod
    def __eval_error(events, column, t, weight, y):
        res = np.empty(y.shape[0],dtype=float)
        left_leaf, right_leaf = events
        indexing = column <= t
        res[indexing] = left_leaf
        res[~indexing] = right_leaf
        w_sum =  weight[res!=y].sum()
        return w_sum if w_sum < 0.5 else -(1 - w_sum)

    def __repr__(self):
        return f'Stump(threshold={self.threshold},column={self.column_name},label=({self.left_label},{self.right_label}))'

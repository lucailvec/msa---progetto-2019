import numpy as np
import pandas as pd


def get_data_frame(path):

    '''
    Drop id and columns after type7
    '''
    print("Path: ", path)
    df = pd.read_csv(path).drop(['Id'], axis=1).copy()
    print("Colonne tot: ", len(df.columns))
    return df
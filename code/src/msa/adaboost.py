from typing import Sequence, Union, Optional

import numpy as np
import pandas as pd
from joblib import Parallel, delayed

from stump import Model
import matplotlib.pyplot as plt

class Adaboost(Model):

    def __init__(self, max_round: int, learning_algo: Model, performance=False, randomized=False):
        self.learning_algo = learning_algo
        self.max_round = max_round

    def _margin(self, x: np.ndarray, y: any) -> float:
        """
        :param x:
        :param y:
        :return: float \in [-1,+1] represents the margin of ensemble on the element (x,y)
        """
        return (y*sum([ a*h.predict(x) for a,h in np.column_stack((self.alpha,self.hypotesis)) if not (h is None)]) )/ self.alpha.sum()
        # return y*(np.sum(self.alpha * [ h.predict(x) for h in self.hypotesis ])/self.alpha.sum())

    def train(self, X: np.ndarray, y: Union[Sequence[any], pd.Series], save_statistics: Optional[bool] = False,
              stops_at_round: Optional[bool] = False):
        """
        The train methods. The behaviour is modified by stops_at_round. Save statistics only create some attribute to inspect the performance during round
        :param X: the X domain
        :param y: the y labels
        :param save_statistics: if true append at each round an entry in training_error, margin_distribution, weight_distribution
        :param stops_at_round: if true after the round, the algorithm suspend the execution and return self.
        This let the user to inspect variable and maybe do some predict to evaluate the algo on the validation/train set.
        If value is False the usual train methods is used instead.
        :return:
        """
        self.weight = np.ones(X.shape[0],dtype='float') / X.shape[0]
        self.alpha = np.zeros(self.max_round,dtype='float')
        self.hypotesis: Sequence[Model] = np.array([None for _ in range(self.max_round)], dtype="object")

        if save_statistics:
            self.training_error = np.zeros(self.max_round)
            self.margin_distribution = np.zeros((self.max_round, X.shape[0]))
            self.weight_distribution = np.zeros((self.max_round, X.shape[0]))

        def inner_funz(X, y, save_statistics, stops_at_round):
            assert all((el in [-1, 1] for el in
                        np.unique(y))), f'y needs to be only -1 or +1 values. But they are {np.unique(y)}'


            for t in range(self.max_round):
                # get best hypotheis h_t
                model = self.learning_algo()
                model.train(X, y, weight=self.weight)
                # calculate error epsilon e_t
                preds = model.predict(X)
                error = self.weight[preds != y].sum()

                # calculate alpha
                alpha = 0.5 * np.log((1 - error) / (error))

                # update every weight and normalize
                sgn_alpha = -alpha * (preds * y)


                self.weight = self.weight * np.exp(sgn_alpha)
                self.weight = self.weight / (self.weight.sum())

                # add new hypo and update structure
                self.hypotesis[t] = model
                self.alpha[t] = alpha

                if save_statistics:
                    self.training_error[t] = (self.predict(X) != y).sum() / X.shape[0]
                    self.margin_distribution[t,:] = np.array([self._margin(row[:-1], row[-1]) for row in np.column_stack((X, y))])
                    self.weight_distribution[t] = self.weight

                if stops_at_round:
                    yield self

        # con il __next__ __iter__ sembrava più incasinato
        if not stops_at_round:
            a = iter(inner_funz(X=X, y=y, save_statistics=save_statistics, stops_at_round=stops_at_round))
            try:
                return next(a)
            except StopIteration:
                pass
        else:
            return inner_funz(X=X, y=y, save_statistics=save_statistics, stops_at_round=stops_at_round)

    def predict(self, x: Union[Sequence[any], np.ndarray]) -> any:
        if len(x.shape) ==1:
            r = sum([ a*h.predict(x) for a, h in np.column_stack((self.alpha, self.hypotesis)) if not (h is None)])
            return +1 if r >= 0 else -1
        else:
            """
            def _inner_funz(a, h):
                return a * h.predict(x)

            resss = Parallel(n_jobs=-1, backend='threading')(
                delayed(_inner_funz)(a, h) for a, h in np.column_stack((self.alpha, self.hypotesis)) if not (h is None))
            """
            resss = [a * h.predict(x) for a, h in np.column_stack((self.alpha, self.hypotesis)) if not (h is None)]
            votes = np.sum(a=resss,axis=0)
            preds = np.empty(votes.shape[0],dtype=int)
            index = votes>=0
            preds[index] = +1
            preds[~index] = -1
            return preds

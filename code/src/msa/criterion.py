# https://www.bogotobogo.com/python/scikit-learn/scikt_machine_learning_Decision_Tree_Learning_Informatioin_Gain_IG_Impurity_Entropy_Gini_Classification_Error.php


import numpy as np
from typing import Sequence, Optional, List


# todo better function with clojure

def gini(p: Sequence[float])->float:
    #assert isinstance(p, np.ndarray)
    return 1 - (p ** 2).sum() if len(p)>0 else 0


def entropy(p: Sequence[float]) ->float:
    #assert isinstance(p, np.ndarray)
    return -(p * np.log2(p)).sum() if len(p)>0 else 0


def classification_error(p: Sequence[float]) -> float:
    return 1 - np.max(p)  if len(p)>0 else 0 # o per le classi binarie min(p)


m = {'gini': gini,
     'entropy': entropy,
     'classification_error': classification_error}


def caos_index( event : List[any], label: Sequence[float], weight : Optional[Sequence[float]] = None, criterion: Optional[str] = 'gini') -> float:
    """
    caos_index calcola data una popolazione ed un criterio l'indice di impurità. Nel caso in cui weight è None il calcolo dell'indice viene fatto in modo "classico" quindi basandosi sul numero di osservazioni e non usando i pesi come probabilità.
    :param event: list of event. What events is defined by prob
    :param weight: None or Sequenza[float]
    :param label: etichette della popolazione Sequence[int|float]
    :param criterion: 'gini' or 'classification_error' or 'entropy'
    :return:
    """
    crit = m[criterion]

    acc_freq = np.array([weight[label == v].sum() for v in event])
    return crit(acc_freq)

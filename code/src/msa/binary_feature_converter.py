from collections.abc import Iterable
import pandas as pd

class Binary_feature_converter:
    """
    Binary_feature_converter
    Convert a pandas dataframe built by some row for the features and some BINARY rows for the y or label (in the one hot encoding).
    It convert the values to the specified values. It works only if the y_i labels are binary or expception occour.
    """

    def __init__(self, pos_class=1, class_values=[1,-1], y_new_name='label', y_names='old_label'):
        """

        :param pos_class: The value or array of values those are contained in the features_collapse
        :param class_values: array of two element; have to follow [positive_label, negative_label].
        :param y_new_name: future name of the label of example
        :param y_names: string or array of string containing the name or names of the columns that they will oerride by y_new_name and will contains only class_values
        """
        assert len(class_values)==2, f'class_values must be 2 values array like, now is {class_values}'
        if not isinstance(class_values,Iterable):
            raise TypeError(f'class_values have to be 2 values array, not {type(class_values)}')
        assert isinstance(y_new_name, str), f'y_new_name have to be a str, not a {type(y_new_name)}'
        assert not isinstance(pos_class,Iterable), f'pos_class ahve to be a single value, not a array like or {type(pos_class)}'
        assert isinstance(y_names, str) or isinstance(y_names, Iterable), f'features_collapse have to be an array or single name of a columns'
        if isinstance(y_names, Iterable):
            assert len(y_names) >= 1, 'features_collapse have to be of 1 or more elements'
        #    assert "" not in y_names, 'features_collapse - empty or "" not allowed '
        else:
            assert not y_names == "", 'name required not empty'

        self.pos_class = pos_class
        self.class_values = class_values
        self.y_new_name = y_new_name
        self.y_names = y_names

    def convert(self,df,target_variable,replace=True):
        assert isinstance(target_variable,str)
        assert isinstance(df,pd.DataFrame)
        assert target_variable in df.columns, 'Target column not in dataFrame'
        assert all([n in df.columns for n in self.y_names]), 'Columns of DataFrame not present, check y_names'

        #loose one hot encoding constraint
        head, tail = self.y_names[0],self.y_names[1:]
        temp = df[head].apply(lambda x: 1 if self.pos_class == x else 0)
        if not isinstance(tail,list):
            tail = [tail]
        for col in tail:
            temp = temp + df[col].apply(lambda x: 1 if self.pos_class == x else 0)

        #per ogni riga i valori devono essere o zero o uno
        t_unique = temp.unique()
        res2 = (0 <= t_unique) * (t_unique <= 1) # product instead of and
        assert res2.all(), 'repeted positive value'


        target = df[target_variable].apply(lambda x : self.class_values[0] if self.pos_class == x else self.class_values[1])
        if replace:
            df = df.drop(self.y_names, axis=1)
        df[self.y_new_name] = target

        return df
    def __repr__(self):
        return f'class_values: {self.class_values}, pos_class: {self.pos_class}, y_names: {self.y_names}, y_new_name: {self.y_new_name}'